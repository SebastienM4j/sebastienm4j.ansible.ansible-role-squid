Ansible Role Squid
==================

Install and configure a squid proxy


Usage
-----

The role install Squid, generate a configuration file from a [template](templates/squid.conf.j2) and configure the service.

### Role Variables

Available variables are listed below, along with default values (see [`defaults/main.yml`](defaults/main.yml)).

#### `squid_acls: []`

List of ACL. The list is grouped and the group generate a comment line in squid configuration.

Each ACL need a name, a type and a value.

```yaml
squid_acls:
  Maven / Gradle:
    - name: maven
      type: dstdomain
      value: .maven.org
    - name: gradle
      type: dstdomain
      value: .gradle.org
```

will generate this :

```shell
# Maven / Gradle
acl maven dstdomain .maven.org
acl gradle dstdomain .gradle.org
```

#### `squid_http_access_allow:`

List of allow HTTP access. The list is grouped and the group generate a comment line in squid configuration.

Default value :

```yaml
squid_http_access_allow:
  Allow access from localhost:
    - localhost
```

will generate :

```shell
http_access allow localhost
```
    
#### `squid_http_port: 3128`

The HTTP port for Squid.

### Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

Ansible Role Iptables is release under [MIT licence](LICENSE).
